export enum LoadStatus {
    INIT,
    IN_PROGRESS,
    SUCCESS,
    ERROR,
    UPDATED,
    CANCELLED
}
