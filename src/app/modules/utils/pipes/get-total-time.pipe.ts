import { Pipe, PipeTransform } from '@angular/core';
import { Shift } from '../../employee/employee.types';

@Pipe({
    name: 'getTotalTime',
})
export class GetTotalTimePipe implements PipeTransform {

    transform(element: Shift): string {
        return String(Number(element.check_out.split(':')[0]) - Number(element.check_in.split(':')[0]));
    }

}
