import { Component, EventEmitter, Input, Output, TemplateRef } from '@angular/core';

@Component({
    selector: 'app-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss', ],
})
export class ModalComponent {
    @Input() template: TemplateRef<any> | null = null;

    @Output() close: EventEmitter<any> = new EventEmitter();

    onClose() {
        this.close.emit();
    }

    stopPropagation(e: Event) {
        e.stopPropagation();
    }
}
