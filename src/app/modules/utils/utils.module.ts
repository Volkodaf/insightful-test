import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalComponent } from './components/modal/modal.component';
import { GetTotalTimePipe } from './pipes/get-total-time.pipe';

@NgModule({
    declarations: [
        ModalComponent,
        GetTotalTimePipe,
    ],
    imports: [
        CommonModule,
    ],
    exports: [
        ModalComponent,
        GetTotalTimePipe,
    ],
})
export class UtilsModule { }
