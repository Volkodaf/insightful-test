import { LoadStatus } from '../../app.constants';

export type DashboardStoreType = {
    loadStatus: LoadStatus;
    totalEmployees: number;
    totalOvertimeAmount: number;
    totalRegularAmount: number;
    totalTime: number;
};
