import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-total-time',
    templateUrl: './total-time.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TotalTimeComponent {
    @Input() value: number | null = null;
}
