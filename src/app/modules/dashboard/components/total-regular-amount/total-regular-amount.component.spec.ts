import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalRegularAmountComponent } from './total-regular-amount.component';

describe('TotalRegularAmountComponent', () => {
  let component: TotalRegularAmountComponent;
  let fixture: ComponentFixture<TotalRegularAmountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TotalRegularAmountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalRegularAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
