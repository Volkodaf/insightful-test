import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-total-regular-amount',
    templateUrl: './total-regular-amount.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TotalRegularAmountComponent {
    @Input() value: number | null = null;
}
