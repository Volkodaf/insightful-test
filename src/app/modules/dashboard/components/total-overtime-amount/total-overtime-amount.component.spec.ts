import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalOvertimeAmountComponent } from './total-overtime-amount.component';

describe('TotalOvertimeAmountComponent', () => {
  let component: TotalOvertimeAmountComponent;
  let fixture: ComponentFixture<TotalOvertimeAmountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TotalOvertimeAmountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalOvertimeAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
