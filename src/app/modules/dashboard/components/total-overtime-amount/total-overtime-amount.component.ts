import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-total-overtime-amount',
    templateUrl: './total-overtime-amount.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TotalOvertimeAmountComponent {
    @Input() value: number | null = null;
}
