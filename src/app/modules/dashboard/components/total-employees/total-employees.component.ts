import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-total-employees',
    templateUrl: './total-employees.component.html',
})
export class TotalEmployeesComponent {
    @Input() value: number | null = null;
}
