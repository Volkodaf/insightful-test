import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { DashboardStore } from '../../services/dashboard.store';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { DashboardStoreType } from '../../dashboard.types';
import { DashboardComponentEnum } from '../../dashboard.constants';

type DashboardElement = {
    value$: Observable<number>;
    component: DashboardComponentEnum;
};

@Component({
    selector: 'app-dashboard-layout',
    templateUrl: './dashboard-layout.container.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardLayoutContainer implements OnInit, OnDestroy {
    elements: Array<DashboardElement> = [
        {
            value$: this.dashboardStore.metadata$.pipe(
                map((state: DashboardStoreType) => state.totalEmployees)
            ),
            component: DashboardComponentEnum.TOTAL_EMPLOYEES,
        },
        {
            value$: this.dashboardStore.metadata$.pipe(
                map((state: DashboardStoreType) => state.totalTime)
            ),
            component: DashboardComponentEnum.TOTAL_TIME,
        },
        {
            value$: this.dashboardStore.metadata$.pipe(
                map((state: DashboardStoreType) => state.totalRegularAmount)
            ),
            component: DashboardComponentEnum.REGULAR_AMOUNT,
        },
        {
            value$: this.dashboardStore.metadata$.pipe(
                map((state: DashboardStoreType) => state.totalOvertimeAmount)
            ),
            component: DashboardComponentEnum.OVERTIME_AMOUNT,
        },
    ];

    loadStatus$ = this.dashboardStore.loadStatus$;

    private readonly subscription = new Subscription();

    constructor(
        private readonly dashboardStore: DashboardStore) { }

    ngOnInit(): void {
        this.subscription.add(this.dashboardStore.metadata$.subscribe());
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}
