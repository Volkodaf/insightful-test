import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { DashboardComponentEnum } from '../../dashboard.constants';
import { LoadStatus } from '../../../../app.constants';

@Component({
    selector: 'app-dashboard-element',
    templateUrl: './dashboard-element.container.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardElementContainer {
    @Input() value: number | null = null;

    @Input() loadStatus: LoadStatus | null = null;

    @Input() componentToResolve?: DashboardComponentEnum;

    LoadStatus = LoadStatus;

    DashboardComponentEnum = DashboardComponentEnum;
}
