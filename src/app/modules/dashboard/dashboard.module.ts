import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardLayoutContainer } from './containers/dashboard-layout/dashboard-layout.container';
import { TotalEmployeesComponent } from './components/total-employees/total-employees.component';
import { TotalTimeComponent } from './components/total-time/total-time.component';
import { TotalRegularAmountComponent } from './components/total-regular-amount/total-regular-amount.component';
import { TotalOvertimeAmountComponent } from './components/total-overtime-amount/total-overtime-amount.component';
import { EmployeeModule } from '../employee/employee.module';
import { MatGridListModule } from '@angular/material/grid-list';
import { DashboardElementContainer } from './containers/dashboard-element/dashboard-element.container';
import { UtilsModule } from '../utils/utils.module';
import { MatCardModule } from '@angular/material/card';


@NgModule({
    declarations: [
        DashboardLayoutContainer,
        TotalEmployeesComponent,
        TotalTimeComponent,
        TotalRegularAmountComponent,
        TotalOvertimeAmountComponent,
        DashboardElementContainer,
    ],
    imports: [
        CommonModule,
        DashboardRoutingModule,
        EmployeeModule,
        MatGridListModule,
        UtilsModule,
        MatCardModule,
    ],
})
export class DashboardModule { }
