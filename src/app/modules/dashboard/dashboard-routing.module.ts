import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardLayoutContainer } from './containers/dashboard-layout/dashboard-layout.container';

const routes: Routes = [
    {
        path: '',
        component: DashboardLayoutContainer,
        loadChildren: () => import('../../modules/employee/employee.module').then(m => m.EmployeeModule),
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes), ],
    exports: [RouterModule, ],
})
export class DashboardRoutingModule { }
