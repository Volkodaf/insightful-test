export enum DashboardComponentEnum {
    TOTAL_EMPLOYEES,
    OVERTIME_AMOUNT,
    REGULAR_AMOUNT,
    TOTAL_TIME
}
