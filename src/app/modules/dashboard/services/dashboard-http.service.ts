import { Injectable } from '@angular/core';
import { HttpService } from '../../../services/http/http.service';
import { Observable } from 'rxjs';
import { DashboardStoreType } from '../dashboard.types';

@Injectable({
    providedIn: 'root',
})
export class DashboardHttpService {

    constructor(private readonly httpService: HttpService) { }

    getMetadata(): Observable<DashboardStoreType> {
        return this.httpService.get<{}, DashboardStoreType>('dashboard');
    }
}
