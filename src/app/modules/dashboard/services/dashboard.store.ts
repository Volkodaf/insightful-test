import { Injectable } from '@angular/core';
import { BehaviorSubject, EMPTY, Observable } from 'rxjs';
import { DashboardStoreType } from '../dashboard.types';
import { DashboardHttpService } from './dashboard-http.service';
import { LoadStatus } from '../../../app.constants';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

const DEFAULT_STATE: DashboardStoreType = {
    loadStatus: LoadStatus.INIT,
    totalEmployees: 0,
    totalOvertimeAmount: 0,
    totalRegularAmount: 0,
    totalTime: 0,
};

@Injectable({
    providedIn: 'root',
})
export class DashboardStore {
    private readonly subject$: BehaviorSubject<DashboardStoreType> = new BehaviorSubject(DEFAULT_STATE);

    private state$: Observable<DashboardStoreType> = this.subject$.asObservable();

    loadStatus$: Observable<LoadStatus> = this.state$.pipe(
        map((state: DashboardStoreType) => state.loadStatus)
    );

    metadata$: Observable<DashboardStoreType> = this.state$.pipe(
        switchMap((state: DashboardStoreType) => {
            if (state.loadStatus === LoadStatus.INIT) {
                this.setStatus(LoadStatus.IN_PROGRESS);
                return this.dashboardHttpService.getMetadata().pipe(
                    catchError(() => {
                        this.setStatus(LoadStatus.ERROR);
                        return EMPTY;
                    }),
                    tap((metadata: Partial<DashboardStoreType>) => {
                        this.setStatus(LoadStatus.SUCCESS);
                        this.update(metadata);
                    }),
                    switchMap(() => this.state$)
                );
            }

            return this.state$;
        })
    );

    constructor(private readonly dashboardHttpService: DashboardHttpService) { }

    setStatus(status: LoadStatus) {
        this.update({
            loadStatus: status,
        });
    }

    update(value: Partial<DashboardStoreType> = {}) {
        this.subject$.next({
            ...this.subject$.getValue(),
            ...value,
        });
    }
}
