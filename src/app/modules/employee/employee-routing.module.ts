import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BulkEditContainer } from './containers/bulk-edit/bulk-edit.container';

const routes: Routes = [
    {
        path: 'employee-bulk-edit',
        component: BulkEditContainer,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes), ],
    exports: [RouterModule, ],
})
export class EmployeeRoutingModule { }
