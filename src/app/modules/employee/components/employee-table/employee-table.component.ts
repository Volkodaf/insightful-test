import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectionModel } from '@angular/cdk/collections';
import { Employee, EmployeeState } from '../../employee.types';
import { EmployeeStoreService } from '../../services/employee-store.service';
import { LoadStatus } from '../../../../app.constants';
import { tap } from 'rxjs/operators';

const initialSelection:Array<Employee> = [];
const allowMultiSelect = true;

@Component({
    selector: 'app-employee-table',
    templateUrl: './employee-table.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmployeeTableComponent implements OnDestroy {
    displayedColumns: Array<string> = [
        'select',
        'name',
        'email',
        'total_clocked_in_time',
        'total_amount_paid',
        'total_overtime_amount',
    ];

    selection = new SelectionModel<Employee>(allowMultiSelect, initialSelection);

    employeeState$ = this.employeeStoreService.employeeState$.pipe(
        tap((employeeState: EmployeeState) => {
            if (employeeState.loadStatus === LoadStatus.UPDATED || employeeState.loadStatus === LoadStatus.CANCELLED) {
                this.selection.clear();
            }
        })
    );

    LoadStatus = LoadStatus;

    constructor(
        private readonly router: Router,
        private readonly route: ActivatedRoute,
        public readonly employeeStoreService: EmployeeStoreService) { }

    ngOnDestroy() {
        this.employeeStoreService.update({
            selectedEmployees: [],
        });
    }

    openBulkEdit() {
        this.employeeStoreService.update({
            selectedEmployees: this.selection.selected,
        });
        this.router.navigate(['employee-bulk-edit', ], {
            relativeTo: this.route,
        });
    }

    isAllSelected(employees: Array<Employee>) {
        const numSelected = this.selection.selected.length;
        const numRows = employees.length;
        return numSelected == numRows;
    }

    masterToggle(employees: Array<Employee>) {
        if (this.isAllSelected(employees)) {
            this.selection.clear();
        } else {
            employees.forEach((row: Employee) => this.selection.select(row));
        }
    }
}
