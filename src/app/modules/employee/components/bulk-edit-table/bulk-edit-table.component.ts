import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Shift } from '../../employee.types';

@Component({
    selector: 'app-bulk-edit-table',
    templateUrl: './bulk-edit-table.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BulkEditTableComponent {
    @Input() shifts: Array<Shift> | null = null;

    displayedColumns: Array<string> = [
        'shift',
        'in_time',
        'out_time',
        'total_time',
    ];
}
