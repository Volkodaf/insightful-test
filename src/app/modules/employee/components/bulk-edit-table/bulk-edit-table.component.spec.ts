import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkEditTableComponent } from './buld-edit-table.component';

describe('BuldEditTableComponent', () => {
  let component: BulkEditTableComponent;
  let fixture: ComponentFixture<BulkEditTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BulkEditTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkEditTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
