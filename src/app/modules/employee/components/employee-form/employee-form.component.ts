import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Employee } from '../../employee.types';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { EmployeeStoreService } from '../../services/employee-store.service';
import { debounceTime, tap } from 'rxjs/operators';

@Component({
    selector: 'app-employee-form',
    templateUrl: './employee-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmployeeFormComponent implements OnInit {
    @Input() employee: Employee | null = null;

    @Output() employeeUpdated: EventEmitter<Employee> = new EventEmitter<Employee>();

    formGroup: FormGroup | undefined;

    private subscription = new Subscription();

    constructor(
        private readonly fb: FormBuilder,
        private readonly employeeStoreService: EmployeeStoreService) {}

    ngOnInit(): void {
        this.formGroup = this.fb.group(<Employee> this.employee);
        this.subscription.add(
            this.formGroup.valueChanges
                .pipe(
                    debounceTime(300),
                    tap((value: Employee) => {
                        value.shifts = this.employee?.shifts || [];
                        this.employeeUpdated.emit(value);
                    })
                ).subscribe()
        );
    }

}
