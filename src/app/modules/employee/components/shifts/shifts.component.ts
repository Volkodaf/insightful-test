import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Shift } from '../../employee.types';

@Component({
    selector: 'app-shifts',
    templateUrl: './shifts.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ShiftsComponent implements OnInit {
    @Input() inputShifts: Array<Shift> = [];

    shifts: Array<Shift> = [];

    shiftDates: Array<string> = [];

    choosedFilter: string | null = null;

    ngOnInit() {
        this.shiftDates = this.inputShifts.map((sh: Shift) => sh.date).filter((value, index, self) => {
            return self.indexOf(value) === index;
        });

        this.choosedFilter = this.inputShifts[0].date;

        this.filterShifts();
    }

    filterShifts() {
        this.shifts = this.inputShifts.filter((shift: Shift) => shift.date == this.choosedFilter);
    }
}
