import { LoadStatus } from '../../app.constants';

export type Employee = {
    id: number;
    name: string;
    email: string;
    total_clocked_in_time: string;
    total_amount_paid: string;
    total_overtime_amount: string;
    status: string;
    hourly_rate: number;
    overtime_hourly_rate: number;
    shifts: Array<Shift>;
};

export type EmployeeState = {
    loadStatus: LoadStatus;
    employees: Array<Employee>;
    selectedEmployees: Array<Employee>;
};

export type Shift = {
    id: number;
    name: string;
    date: string;
    check_in: string;
    check_out: string;
};
