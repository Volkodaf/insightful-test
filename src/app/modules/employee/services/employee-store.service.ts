import { Injectable } from '@angular/core';
import { LoadStatus } from '../../../app.constants';
import { Employee, EmployeeState } from '../employee.types';
import { BehaviorSubject, EMPTY } from 'rxjs';
import { EmployeeHttpService } from './employee-http.service';
import { catchError, switchMap, tap } from 'rxjs/operators';

const DEFAULT_STATE: EmployeeState = {
    loadStatus: LoadStatus.INIT,
    employees: [],
    selectedEmployees: [],
};

const loadFromServerStatuses = [
    LoadStatus.INIT,
];

const updateOnServerStatuses = [
    LoadStatus.UPDATED,
];

const cancelledStatuses = [
    LoadStatus.CANCELLED,
];

@Injectable({
    providedIn: 'root',
})
export class EmployeeStoreService {
    private readonly subject$: BehaviorSubject<EmployeeState> = new BehaviorSubject(DEFAULT_STATE);

    private state$ = this.subject$.asObservable();

    employeeState$ = this.state$.pipe(
        switchMap((state: EmployeeState) => {
            if (cancelledStatuses.includes(state.loadStatus)) {
                setTimeout(() => {
                    this.setStatus(LoadStatus.SUCCESS);
                }, 2000);
                return this.state$;
            }
            if (updateOnServerStatuses.includes(state.loadStatus)) {
                // Will not call update because now we just need get updated state
                // this.employeeHttpService.bulkUpdateEmployees(state.employees)
                setTimeout(() => {
                    this.setStatus(LoadStatus.SUCCESS);
                }, 2000);
                return this.state$;
            }
            if (loadFromServerStatuses.includes(state.loadStatus)) {
                return this.employeeHttpService.getEmployees().pipe(
                    catchError(() => {
                        this.setStatus(LoadStatus.ERROR);
                        return EMPTY;
                    }),
                    tap((employees: Array<Employee>) => {
                        this.setStatus(LoadStatus.SUCCESS);
                        this.update({
                            employees,
                        });
                    }),
                    switchMap(() => this.state$)
                );
            }

            return this.state$;
        })
    );

    constructor(private readonly employeeHttpService: EmployeeHttpService) { }

    setStatus(loadStatus: LoadStatus) {
        this.update({
            loadStatus,
        });
    }

    update(value: Partial<EmployeeState> = {}) {
        this.subject$.next({
            ...this.subject$.getValue(),
            ...value,
        });
    }

    updateEmployees(updatedEmployees: Array<Employee>) {
        const value = this.subject$.getValue();
        const employees = value.employees;
        updatedEmployees.forEach((updateEmployee: Employee) => {
            employees.splice(
                value.employees.findIndex((exEmployee: Employee) => exEmployee.id === updateEmployee.id),
                1,
                updateEmployee
            );
        });


        this.update({
            employees: [...employees, ], loadStatus: LoadStatus.UPDATED,
        });
    }
}
