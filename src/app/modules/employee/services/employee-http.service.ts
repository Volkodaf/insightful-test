import { Injectable } from '@angular/core';
import { HttpService } from '../../../services/http/http.service';
import { Observable } from 'rxjs';
import { Employee } from '../employee.types';

@Injectable({
    providedIn: 'root',
})
export class EmployeeHttpService {

    constructor(private readonly httpService: HttpService) { }

    getEmployees(): Observable<Array<Employee>> {
        return this.httpService.get<{}, Array<Employee>>('employee');
    }

    bulkUpdateEmployees(employees: Array<Employee>): Observable<Array<Employee>> {
        return this.httpService.put<{ employees: Array<Employee>; }, Array<Employee>>('employee', {
            employees,
        });
    }
}
