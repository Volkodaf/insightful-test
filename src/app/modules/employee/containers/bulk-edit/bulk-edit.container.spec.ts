import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkEditContainer } from './bulk-edit.component';

describe('BulkEditComponent', () => {
  let component: BulkEditContainer;
  let fixture: ComponentFixture<BulkEditContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BulkEditContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkEditContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
