import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeStoreService } from '../../services/employee-store.service';
import { Employee, EmployeeState } from '../../employee.types';
import { map, tap } from 'rxjs/operators';
import { LoadStatus } from '../../../../app.constants';

@Component({
    selector: 'app-bulk-edit',
    templateUrl: './bulk-edit.container.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BulkEditContainer {
    employees$ = this.employeeStoreService.employeeState$.pipe(
        map((state: EmployeeState) => state.selectedEmployees),
        tap((selectedEmployees: Array<Employee>) => {
            if (!selectedEmployees.length) {
                this.closeModal(false);
            }
        })
    );

    private updatedEmployees: Array<Employee> = [];

    constructor(
        private readonly router: Router,
        private readonly employeeStoreService: EmployeeStoreService) { }

    closeModal(setCancelStatus: boolean = true): void {
        const urlArr = this.router.url.split('/');
        urlArr.pop();
        this.router.navigate(['/' + urlArr.join('/'), ]);
        if (setCancelStatus) {
            this.employeeStoreService.setStatus(LoadStatus.CANCELLED);
        }
    }

    employeeUpdated(employee: Employee) {
        const index = this.updatedEmployees.findIndex((updatedEmployee: Employee) => updatedEmployee.id === employee.id);
        if (~index) {
            this.updatedEmployees.splice(index, 1, employee);
        } else {
            this.updatedEmployees.push(employee);
        }
    }

    save() {
        this.employeeStoreService.updateEmployees(this.updatedEmployees);
        this.closeModal();
    }
}
