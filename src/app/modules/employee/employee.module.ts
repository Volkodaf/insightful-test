import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeRoutingModule } from './employee-routing.module';
import { EmployeeTableComponent } from './components/employee-table/employee-table.component';
import { UtilsModule } from '../utils/utils.module';
import { MatTableModule } from '@angular/material/table';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { BulkEditTableComponent } from './components/bulk-edit-table/bulk-edit-table.component';
import { ShiftsComponent } from './components/shifts/shifts.component';
import { BulkEditContainer } from './containers/bulk-edit/bulk-edit.container';
import { EmployeeFormComponent } from './components/employee-form/employee-form.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        EmployeeTableComponent,
        BulkEditContainer,
        BulkEditTableComponent,
        ShiftsComponent,
        EmployeeFormComponent,
    ],
    imports: [
        CommonModule,
        EmployeeRoutingModule,
        UtilsModule,
        MatTableModule,
        MatCheckboxModule,
        MatButtonModule,
        MatGridListModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        ReactiveFormsModule,
    ],
    exports: [
        EmployeeTableComponent,
    ],
})
export class EmployeeModule { }
