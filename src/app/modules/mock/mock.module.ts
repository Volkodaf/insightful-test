import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpMockInterceptor } from './services/http-mock.interceptor';

@NgModule({
    declarations: [],
    imports: [],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpMockInterceptor,
            multi: true,
        },
    ],
})
export class MockModule { }
