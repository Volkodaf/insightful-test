import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import mockConfig from '../mock.config';
import { delay } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class HttpMockInterceptor implements HttpMockInterceptor {
    intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        if ((<any>mockConfig)[request.method] && (<any>mockConfig)[request.method][request.url]) {
            const delayValue = Math.floor(Math.random() * (500 - 300)) + 300;
            return (<any>mockConfig)[request.method][request.url].handler().pipe(
                delay(delayValue)
            );
        }

        return next.handle(request);
    }
}
