import { of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
// @ts-ignore
import dashboardMock from './data/dashboard.mock.json';
// @ts-ignore
import employeeMock from './data/employee.mock.json';


function getDashboard() {
    return of(new HttpResponse({
        status: 200,
        body: dashboardMock,
    }));
}

function getEmployees() {
    return of(new HttpResponse({
        status: 200,
        body: employeeMock,
    }));
}

export default {
    GET: {
        '/api/dashboard': {
            handler: getDashboard,
        },
        '/api/employee': {
            handler: getEmployees,
        },
    },
    POST: {},
};
