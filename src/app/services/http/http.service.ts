import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class HttpService {
    private readonly api = '/api/';

    constructor(private readonly httpClient: HttpClient) { }

    get<U, T>(path: string, queryParams: U = {} as U): Observable<T> {
        return this.httpClient.get<T>(this.preparePath(path), queryParams);
    }

    post<U, T>(path: string, body: U): Observable<T> {
        return this.httpClient.post<T>(this.preparePath(path), body);
    }

    put<U, T>(path: string, body: U): Observable<T> {
        return this.httpClient.put<T>(this.preparePath(path), body);
    }

    private preparePath(path: string): string {
        return this.api + path;
    }
}
